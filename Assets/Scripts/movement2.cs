﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement2 : MonoBehaviour
{
    // Start is called before the first frame update
    public Rigidbody rb;
    public GameObject cam;

    public float mouseSensitivity =1;
    public float speed = 1;

    float upRotation=0;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        print("thing");
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));

        float mouseX = Input.GetAxis("Mouse X") * (mouseSensitivity* 100)* Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * (mouseSensitivity* 100)* Time.deltaTime;

        upRotation -= mouseY;

        if(upRotation > 90)
        {
            upRotation = 90;
        }else if (upRotation < -90) 
        {
            upRotation = -90;
        }

        cam.transform.localRotation = Quaternion.Euler(upRotation,0,0);
        transform.Rotate(0,mouseX,0,Space.Self);


        //print(cam.transform.localEulerAngles.x);
        //if())

        transform.Translate(movement*Time.deltaTime*speed,Space.Self);

    }
}
